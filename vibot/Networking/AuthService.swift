import Foundation

/// Runs query data task, and stores results in array of Tracks
class AuthService {
    
    let session = URLSession.shared
    let userURL = URL(string: "https://visibot.ibl.pw/api/v1/auth/user/")!
    let authURL = URL(string: "https://visibot.ibl.pw/api/v1/auth/login/")!
    
    
    func getUser(token: String, cbk: ((_: [String: Any]) -> Void)?) {
        var request = URLRequest(url: userURL)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Token " + token, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request) { (data, responseData, err) in
            if let response = responseData as? HTTPURLResponse {
                let statusCode = response.statusCode
                print("Response code: \(statusCode)")
//                print(response)
//                print(String(data: (data)!, encoding: String.Encoding.utf8))
//                print(err ?? "No error")
                do {
                    cbk!(try (JSONSerialization.jsonObject(with: (data)!, options: []) as? [String: Any] ?? ["NULL": true]))
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    func sendCredentials(username: String, password: String, cbk: ((_: [String: Any]) -> Void)?) {
        var request = URLRequest(url: authURL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let json = [
            "username": username,
            "password": password
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        let task = session.uploadTask(with: request, from: jsonData) { (data, responseData, err) in
            if let response = responseData as? HTTPURLResponse {
                let statusCode = response.statusCode
                print("Response code: \(statusCode)")
//                print(response)
//                print(String(data: (data)!, encoding: String.Encoding.utf8))
//                print(err ?? "No error")
                do {
                    cbk!(try (JSONSerialization.jsonObject(with: (data)!, options: []) as? [String: Any] ?? ["NULL": true]))
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
  
}
