import Foundation

struct eventResponseItem: Codable {
    let id: Int
    struct Token: Codable {
        let id: Int
        struct Entry: Codable {
            let id: Int
            let name: String
            let date: String
            let token: Int
        }
        let entries: [Entry]
        let code: String
        let date: String
        let event: Int
    }
    let tokens: [Token]
    let title: String
    let owner: Int
}


/// Runs query data task, and stores results in array of Tracks
class EventsService {
    
    let session = URLSession.shared
    let eventsURL = URL(string: "https://visibot.ibl.pw/api/v1/event/")!
    
    
    func getEvents(token: String, cbk: ((_: [eventResponseItem]) -> Void)?) {
        var request = URLRequest(url: eventsURL)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Token " + token, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request) { (data, responseData, err) in
            if let response = responseData as? HTTPURLResponse {
                let statusCode = response.statusCode
                print("Response code: \(statusCode)")
//                print(response)
//                print(String(data: (data)!, encoding: String.Encoding.utf8))
//                print(err ?? "No error")
                do {
                    let decoder = JSONDecoder()
                    let dt = try decoder.decode([eventResponseItem].self, from: (String(data: (data)!, encoding: String.Encoding.utf8)?.data(using: .utf8))!)
                    cbk!(dt)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    func createEvent(token: String, title: String, cbk: ((_: Data) -> Void)?) {
        var request = URLRequest(url: eventsURL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Token " + token, forHTTPHeaderField: "Authorization")
        let json = [
            "title": title
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        let task = session.uploadTask(with: request, from: jsonData) { (data, responseData, err) in
            if let response = responseData as? HTTPURLResponse {
                let statusCode = response.statusCode
                print("Response code: \(statusCode)")
//                print(response)
//                print(String(data: (data)!, encoding: String.Encoding.utf8))
//                print(err ?? "No error")
                do {
                    cbk!(data!)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    func editEvent(token: String, title: String, event: Int, cbk: ((_: Data) -> Void)?) {
        let eventsURL = URL(string: "https://visibot.ibl.pw/api/v1/event/\(event)/")!
        var request = URLRequest(url: eventsURL)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Token " + token, forHTTPHeaderField: "Authorization")
        let json = [
            "title": title
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        let task = session.uploadTask(with: request, from: jsonData) { (data, responseData, err) in
            if let response = responseData as? HTTPURLResponse {
                let statusCode = response.statusCode
                print("Response code: \(statusCode)")
//                print(response)
//                print(String(data: (data)!, encoding: String.Encoding.utf8))
//                print(err ?? "No error")
                do {
                    cbk!(data!)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    func deleteEvent(token: String, event: Int, cbk: ((_: Data) -> Void)?) {
        let eventsURL = URL(string: "https://visibot.ibl.pw/api/v1/event/\(event)/")!
        var request = URLRequest(url: eventsURL)
        request.httpMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Token " + token, forHTTPHeaderField: "Authorization")
        
        let task = session.dataTask(with: request) { (data, responseData, err) in
            if let response = responseData as? HTTPURLResponse {
                let statusCode = response.statusCode
                print("Response code: \(statusCode)")
//                print(response)
//                print(String(data: (data)!, encoding: String.Encoding.utf8))
//                print(err ?? "No error")
                do {
                    cbk!(data!)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    func createToken(token: String, event: Int, cbk: ((_: Data) -> Void)?) {
        let eventsURL = URL(string: "https://visibot.ibl.pw/api/v1/tokencreate/")!
        var request = URLRequest(url: eventsURL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Token " + token, forHTTPHeaderField: "Authorization")
        let json = [
            "event": event
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
        
        let task = session.uploadTask(with: request, from: jsonData) { (data, responseData, err) in
            if let response = responseData as? HTTPURLResponse {
                let statusCode = response.statusCode
                print("Response code: \(statusCode)")
//                print(response)
//                print(String(data: (data)!, encoding: String.Encoding.utf8))
//                print(err ?? "No error")
                do {
                    cbk!(data!)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
}
