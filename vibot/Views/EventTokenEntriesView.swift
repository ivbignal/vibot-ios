//
//  PlaceDetailView.swift
//  SwiftUIStarterKitApp
//
//  Created by Osama Naeem on 11/08/2019.
//  Copyright © 2019 NexThings. All rights reserved.
//

import SwiftUI
import Combine

struct EventTokenEntriesView : View {
    @Binding var isShowing: Bool
    @Binding var token: EventTokenItem?
    
    @State var entries: [EntryItem] = []
    @State var updating: Bool = true
    
    var body: some View {
        GeometryReader { g in
            ZStack {
                Image("token")
                    .resizable()
                    .frame(width: g.size.width, height: g.size.height)
                    .aspectRatio(contentMode: .fit)
                    .opacity(0.3)
                    .background(Color.black)
                    .onDisappear {
                        self.isShowing = false
                }
                
                VStack(alignment: .leading) {
                    Text(self.token?.code ?? "")
                        .foregroundColor(Color.white)
                        .font(.system(size: 30, weight: .bold, design: .default))
                        .padding(.top, 34)
                        .padding(.leading, 30)
                    
                    ScrollView (.vertical, showsIndicators: false) {
                        VStack (alignment: .leading, spacing: 20) {
                            ForEach(self.entries, id: \.id) { item in
                                EntryDetail(entry: item)
                                    .frame(width: g.size.width - 20, height: 55)
                            }
                        }.padding(.leading, 5)
                    }
                    
                }
            }
        }
        .edgesIgnoringSafeArea(.bottom)
        .onAppear {
            self.updating = true
            reFetch()
        }
        .onDisappear {
            self.updating = false
        }
    }
    
    func reFetch() {
        print("Loading " + (token?.code ?? "some token"))
                    let svc = EventsService()
                    svc.getEvents(token: UserDefaults.standard.string(forKey: "AccessToken") ?? "0") { (rData) in
                        var entriesResponse: [EntryItem] = []
                        rData.forEach { (el) in
                            el.tokens.forEach { (t) in
                                if t.code == self.token?.code {
                                    t.entries.forEach { (e) in
                                        entriesResponse.append(EntryItem(id: e.id, name: e.name, date: formatDate(date: e.date)))
                                    }
                                }
                            }
                        }
                        self.entries = entriesResponse
                    }
        if self.updating {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                reFetch()
            }
        }
       
    }
    
    func formatDate(date: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
        return formatter.date(from: date)!
    }
}




struct EntryDetail: View {
    var entry: EntryItem
    
    var body: some View {
        GeometryReader { g in
        VStack(alignment: .leading) {
            Text(self.formatDate(date: entry.date))
                .foregroundColor(Color.white)
                .font(.system(size: 16, weight: .regular, design: .default))
                .padding(.leading, 5)
            
            Text(entry.name)
                .foregroundColor(Color.white)
                .font(.system(size: 24, weight: .bold, design: .default))
                .padding(.leading, 5)
        }.frame(width: g.size.width - 10, alignment: .leading)
        }
    }
    
    func formatDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: date)
    }
}
