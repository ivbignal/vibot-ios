//
//  AccountView.swift
//  SwiftUIStarterKitApp
//
//  Created by Osama Naeem on 08/08/2019.
//  Copyright © 2019 NexThings. All rights reserved.
//

import SwiftUI

struct AccountView: View {
    @EnvironmentObject var settings: UserSettings
    
    @State var notificationToggle: Bool = false
    @State var locationUsage: Bool = false
    @State var username: String = "James"
    @State var selectedCurrency: Int = 0
    @State var currencyArray: [String] = ["$ US Dollar", "£ GBP", "€ Euro"]
    
    @State var selectedPaymentMethod: Int = 1
    @State var paymentMethodArray: [String] = ["Paypal", "Credit/Debit Card", "Bitcoin"]
    
    @Binding var loggedIn: Bool
    
    var body: some View {
        GeometryReader { g in
            VStack {
                Image("profile")
                    .resizable()
                    .frame(width: 100, height: 100)
                    .background(Color.yellow)
                    .clipShape(Circle())
                    .padding(.bottom, 10)
                Text(self.settings.username)
                    .font(.system(size: 20))
                    
                Form {
                    
                    Section(header: Text("Аккаунт")) {
                       NavigationLink(destination:
                            VStack {
                                Text("Вы действительно хотите выйти?")
                                Button(action: {
                                    print("Выход")
                                    UserDefaults.standard.set("0", forKey: "AccessToken")
                                    loggedIn = false
                                }) {
                                       HStack {
                                           Text("Выйти")
                                       }
                                           .padding()
                                           .frame(width: g.size.width - 40, height: 40)
                                           .foregroundColor(Color.white)
                                           .background(Color.red)
                                           .cornerRadius(5)
                                   }
                                .padding(.top, 20)
                            }.padding()
                       ) {
                            Text("Выйти")
                            .foregroundColor(.red)
                        }

                    }
                    
//                    Section(footer: Text("Allow push notifications to get latest travel and equipment deals")) {
//                        Toggle(isOn: self.$locationUsage) {
//                              Text("Location Usage")
//                        }
//                        Toggle(isOn: self.$notificationToggle) {
//                            Text("Notifications")
//                        }
//                    }
                        
            }.background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
            .navigationBarTitle("Профиль")
         }
            .padding(.top, 60)
        }
    }
 }

struct AccountView_Previews: PreviewProvider {
        static var previews: some View {
                StatefulPreviewWrapper(false) {
                    AccountView(loggedIn: $0)
                        .preferredColorScheme(.dark)
                }
            }
}

