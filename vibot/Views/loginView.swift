//
//  LogInView.swift
//  SwiftUIStarterKitApp
//
//  Created by Osama Naeem on 02/08/2019.
//  Copyright © 2019 NexThings. All rights reserved.
//

import SwiftUI

struct LogInView: View {
    @EnvironmentObject var settings: UserSettings
    @State private var AccessToken = UserDefaults.standard.string(forKey: "AccessToken")
    
    @State  private var username: String = ""
    @State  private var password: String = ""
    
    @Binding var loggedIn: Bool
    
    var body: some View {
    GeometryReader { geometry in
            VStack (alignment: .center){
                HStack {
                    Image("Logo")
                    .resizable()
                    .frame(width: 20, height: 20)
                    Text("Посещабот")
                        .font(.system(size: 12))
                        
                }.padding(.top, 30)
                .padding(.bottom, 10)
                
                Text("Log Into Your Account")
                    .font(.title)
                    .font(.system(size: 14, weight: .bold, design: Font.Design.default))
                    .padding(.bottom, 50)
                
                TextField("Имя пользователя", text: $username)
                    .frame(width: geometry.size.width - 45, height: 50)
                    .textContentType(.emailAddress)
                    .padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 0))
                    .background(Color("InputBackground"))
                    .cornerRadius(5)
                
                
                SecureField("Пароль", text: $password)
                    .frame(width: geometry.size.width - 45, height: 50)
                    .padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 0))
                    .foregroundColor(.gray)
                    .accentColor(.red)
                    .background(Color("InputBackground"))
                    .textContentType(.password)
                    .cornerRadius(5)
                
                 Button(action: {
                    let auth = AuthService()
                    auth.sendCredentials(username: username, password: password) { (rData) in
                        if ((rData["token"] ?? "0") as! String).count == 64 {
                            self.AccessToken = ((rData["token"] ?? "0" ) as! String)
                            UserDefaults.standard.set(self.AccessToken, forKey: "AccessToken")
                            print(UserDefaults.standard.string(forKey: "AccessToken")!)
                            self.loggedIn = true
                        }
                    }
//                    self.settings.loggedIn = true
                 }) {
                        HStack {
                            Text("Log In")
                        }
                            .padding()
                            .frame(width: geometry.size.width - 40, height: 40)
                            .foregroundColor(Color.white)
                            .background(Color.blue)
                            .cornerRadius(5)
                    }
                     .padding(.bottom, 40)
                
                Divider()
                
                Button(action: {
                        print("Take to forget password VC")
                        }) {
                        Text("Forgot your password?")
                }
                
                Spacer()
                
            }
            .padding(.bottom, 90)
    }.onAppear {
        print(self.AccessToken ?? "0")
        if self.AccessToken?.count == 64 {
            let auth = AuthService()
            auth.getUser(token: self.AccessToken!) { (rData) in
                print(rData)
                DispatchQueue.main.async {
                    self.settings.username = rData["username"] as? String ?? ""
                    self.settings.userID = rData["id"] as? Int ?? 0
                }
                loggedIn = true
            }
        } else {
//            let auth = AuthService()
//            auth.getToken()
        }
    }
    }
}

struct LoginView_Previews: PreviewProvider {
        static var previews: some View {
                StatefulPreviewWrapper(false) {
                    LogInView(loggedIn: $0)
                        .preferredColorScheme(.dark)
                }
            }
}



