//
//  ShopContentView.swift
//  SwiftUIStarterKitApp
//
//  Created by Osama Naeem on 06/08/2019.
//  Copyright © 2019 NexThings. All rights reserved.
//

import SwiftUI
import Combine

struct EventItem {
    var id: Int
    var title: String
    var tokens: [EventTokenItem]
}

struct EventTokenItem {
    var id: Int
    var code: String
    var date: Date
}

struct EntryItem {
    var id: Int
    var name: String
    var date: Date
}


struct EventsView: View {
    @EnvironmentObject var settings: UserSettings
    
    @State var isShowing: Bool = false
    @State var placeItemSelected: EventTokenItem? = nil
    
    @State var events: [EventItem] = []
    
    var body: some View {
        GeometryReader { g in
            ScrollView{
                VStack(alignment: .leading) {
                        
                        VStack (spacing: 20) {
                            ForEach(self.events, id: \.id) { item in
                                EventView(event: item, reFetch: self.reFetch, placeItemSelected: self.$placeItemSelected, isShowing: self.$isShowing)
                                         .frame(width: g.size.width - 30, height: 180)
                            }
                        }.padding(.leading, 15)
                        
                        
                    }
                    .navigationBarTitle("События")
                    .navigationBarItems(trailing:
                    Button(action: {
                        print("Add")
                        inputAlert()
                    }) {
                        Text("Новое событие")
                    })
            }.sheet(isPresented: self.$isShowing) { EventTokenEntriesView(isShowing: self.$isShowing, token: self.$placeItemSelected)}
        }
        .onAppear {
            self.reFetch()
        }
    }
    
    func reFetch() {
        let svc = EventsService()
                    svc.getEvents(token: UserDefaults.standard.string(forKey: "AccessToken") ?? "0") { (rData) in
                        var eventsResponse: [EventItem] = []
                        rData.forEach { (el) in
                            var newItem: EventItem = EventItem(id: el.id, title: el.title, tokens: [])
                            var newTokens: [EventTokenItem] = []
                            el.tokens.forEach { (token) in
                                newTokens.append(EventTokenItem(id: token.id, code: token.code, date: self.formatDate(date: token.date)))
                            }
                            newItem.tokens = newTokens
                            eventsResponse.append(newItem)
                        }
                        self.events = eventsResponse.reversed()
                    }
        
    }
    
    func formatDate(date: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
        return formatter.date(from: date)!
    }
    
    private func inputAlert() {
        let alert = UIAlertController(title: "Новое событие", message: "Введите заголовок нового события и нажмите кнопку Добавить", preferredStyle: .alert)
        alert.addTextField() { textField in
            textField.placeholder = "Заголовок события"
        }
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel) { _ in })
        alert.addAction(UIAlertAction(title: "Добавить", style: .default) { txt in
            let txt: String = (alert.textFields![0] as UITextField).text ?? "";
            let svc = EventsService()
            svc.createEvent(token: UserDefaults.standard.string(forKey: "AccessToken") ?? "0", title: txt) { (rData) in
                self.reFetch()
            }
        })
        showAlert(alert: alert)
    }
}

struct TokenButtonView: View {
    
    var token: EventTokenItem
    
    var body: some View {
            ZStack{
                Image("token").renderingMode(.original)
                        .resizable()
                        .frame(width: 155, height: 55)
                        .background(Color.black)
                        .cornerRadius(10)
                        .opacity(0.8)
                        .aspectRatio(contentMode: .fill)
               
                VStack (alignment: .leading) {
                    Spacer()
                    
                    Text(self.token.code)
                        .foregroundColor(Color.white)
                        .font(.system(size: 20, weight: .bold, design: Font.Design.default))
                        .padding(.bottom, 0)
                    Text(self.formatDate(date: self.token.date))
                        .foregroundColor(Color.white)
                        .font(.system(size: 14, weight: .light, design: Font.Design.default))
                        .padding(.bottom, 10)
                }
                    
                
            }
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
          
    }
    
    func formatDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: date)
    }
}

struct EventView: View {
    var event: EventItem
    var reFetch:() -> Void
    
    @Binding var placeItemSelected: EventTokenItem?
    @Binding var isShowing: Bool
    
    var body: some View {
        GeometryReader { g in
            ZStack {
                VStack (alignment: .leading){
                    Text(self.event.title)
                        .padding(.top, 18)
                        .padding(.leading, 18)
                        .font(.system(size: 20, weight: .bold, design: Font.Design.default))
                        .foregroundColor(Color.black)
                    Text("Токенов: \(self.event.tokens.count)")
                        .padding(.leading, 18)
                        .padding(.trailing, 18)
                        .font(.system(size: 14))
                        .foregroundColor(Color.black)
                    
                        
                    ScrollView (.horizontal, showsIndicators: false) {
                        HStack (spacing: 10) {
                            
                            ForEach(self.event.tokens, id: \.id) { item in
                                Button(action: {
                                    self.placeItemSelected = item
                                    self.isShowing = true
                                }) {
                                    TokenButtonView(token: item)
                                                        .frame(width: 155, height: 55)
                                }
                            }
                            VStack (alignment: .leading) {
                                Spacer()
                                
                                Button("+", action: {
                                    let svc = EventsService()
                                    svc.createToken(token: UserDefaults.standard.string(forKey: "AccessToken") ?? "0", event: self.event.id) { (rData) in
                                        self.reFetch()
                                    }
                                })
                                    .foregroundColor(Color.white)
                                    .font(.system(size: 20, weight: .bold, design: Font.Design.default))
                                    .padding(.bottom, 16)
                            }.frame(width: 55, height: 55).background(Color.blue).cornerRadius(10)
                            
                        }.padding(.leading, 18)
                        .padding(.trailing, 18)
                            .padding(.top, 0)
                    }
                    HStack (spacing: 60) {
                        Button("Удалить", action: {
                            deleteAlert()
                        }).foregroundColor(.red).font(.system(size: 18, weight: .semibold, design: Font.Design.default))
                        Button("Редактировать", action: {
                            editAlert()
                        }).font(.system(size: 18, weight: .semibold, design: Font.Design.default))
                    }.padding(.leading, 10).padding(.trailing, 10).padding(.top, 5).frame(width: g.size.width)
                    
                     Spacer()
                }
                }.frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                .background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
                .cornerRadius(10)
                
            }
        }
    
    private func deleteAlert() {
        let alert = UIAlertController(title: "Удаление события", message: "Вы действительно хотите удалить событие \"\(self.event.title)\"", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel) { _ in })
        alert.addAction(UIAlertAction(title: "Удалить", style: .destructive) { txt in
            let svc = EventsService()
            svc.deleteEvent(token: UserDefaults.standard.string(forKey: "AccessToken") ?? "0", event: self.event.id) { (rData) in
                self.reFetch()
            }
        })
        showAlert(alert: alert)
    }
    
    private func editAlert() {
        let alert = UIAlertController(title: "Редактирование события", message: "Внесите необходимые изменения и нажмите кнопку Добавить", preferredStyle: .alert)
        alert.addTextField() { textField in
            textField.placeholder = "Заголовок события"
            textField.text = self.event.title
        }
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel) { _ in })
        alert.addAction(UIAlertAction(title: "Сохранить", style: .default) { txt in
            let txt: String = (alert.textFields![0] as UITextField).text ?? "";
            let svc = EventsService()
            svc.editEvent(token: UserDefaults.standard.string(forKey: "AccessToken") ?? "0", title: txt, event: self.event.id) { (rData) in
                self.reFetch()
            }
        })
        showAlert(alert: alert)
    }
}

struct EventsView_Previews: PreviewProvider {
    static var previews: some View {
        EventsView()
    }
}

