//
//  TabBarView.swift
//  SwiftUIStarterKitApp
//
//  Created by Osama Naeem on 02/08/2019.
//  Copyright © 2019 NexThings. All rights reserved.
//

import SwiftUI

struct TabbarView: View {
    @Binding var loggedIn: Bool
    
    var body: some View {
        TabView {
            NavigationView {
                EventsView()
            }
            .tag(0)
            .tabItem {
                Image("nav-events")
                    .resizable()
                Text("События")
            }
            
            NavigationView {
                AccountView(loggedIn: $loggedIn)
                  }
                   .tag(1)
                    .tabItem {
                    Image("nav-profile")
                        .frame(width: 40, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    Text("Профиль")
                }
        }
    }
}

struct TabBarView_Previews: PreviewProvider {
        static var previews: some View {
                StatefulPreviewWrapper(false) {
                    TabbarView(loggedIn: $0)
                        .preferredColorScheme(.dark)
                }
            }
}

